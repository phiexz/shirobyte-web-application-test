<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("user");
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->dbforge();
        $this->load->helper('html');
		$this->load->helper('url');
    }

    public function get() {
        $data["users"] = $this->user->getAll();

        //return json
        //add the header here
        header('Content-Type: application/json');
        echo json_encode($data["users"]);
    }

    public function count() {
        $count = $this->user->count();
        echo $count;
    }

    public function add() {
        $add = $this->user->add();
        echo $add;
    }

    public function delete() {
        $user = $this->user;
        if($user->delete()){
            echo "success";
        }
        else{
            echo "error";
        }
    }
}