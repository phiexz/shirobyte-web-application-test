<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		$data['title'] = "Welcome page";
		$data['header'] = "Hello, and Welcome";
		$data['subHeader'] = 'This website is created for shirobyte web application test<br>There is <b><span id="count-users">??</span></b> users registered';
		$data['headerAlert'] = "<strong>Heads up!</strong> To begin, please choose menu from navigation bar above :)";

		$this->load->helper('html');
		$this->load->helper('url');
		$this->load->view('index', $data);
	}

	public function register() {
		$data['title'] = "Register page";
		$data['header'] = "User Form";
		$data['subHeader'] = "Fill the form below and click submit to add user";

		$this->load->helper('html');
		$this->load->helper('url');
		$this->load->view('register', $data);
	}

	public function list() {
		$data['title'] = "User list";
		$data['header'] = "User List";
		$data['subHeader'] = "List all user from database, and ability to delete it";

		$this->load->helper('html');
		$this->load->helper('url');
		$this->load->view("list", $data);
}
}
