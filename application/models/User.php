<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
  private $_table = "users";

  public $email;
  public $password;
  public $name;
  public $gender;
  public $telephone;
  public $occupation;

  public function createTable() {
    $fields = array(
            'email' => array(
                    'type' => 'TEXT'
            ),
            'password' => array(
                    'type' => 'TEXT'
            ),
            'name' => array(
                    'type' => 'TEXT'
            ),
            'gender' => array(
                    'type' => 'TEXT'
            ),
            'telephone' => array(
                    'type' => 'TEXT'
            ),
            'occupation' => array(
                    'type' => 'TEXT'
            )
    );
    //add field to table
    $this->dbforge->add_field($fields);
    //add table if not exist
    $this->dbforge->create_table($this->_table, TRUE);
  }

  public function getAll() {
    //create table if not exist
    $this->createTable();

    //limiting column to get
    $this->db->select('email, name, gender, telephone, occupation');
    return $this->db->get($this->_table)->result();
  }

  public function count() {
    //create table if not exist
    $this->createTable();
    
    return $this->db->count_all($this->_table);
  }

  public function add() {
    $post = $this->input->post();
    $this->email = $post["email"];
    $this->password = $post["password"];
    $this->name = $post["name"];
    $this->gender = $post["gender"];
    $this->telephone = $post["telephone"];
    $this->occupation = $post["occupation"];

    //hash password
    $this->password = password_hash($this->password, PASSWORD_BCRYPT);

    //create table if not exist
    $this->createTable();

    //check if email already exist
    $checkExist = $this->db->get_where($this->_table, array('email' => $this->email), 1)->result();
    if($checkExist){
      return "exist";
    }
    else{
      //insert into db
      $insertUser = $this->db->insert($this->_table, $this);
      if($insertUser){
        return "success";
      }
      else{
        return "error";
      }
    }
  }

  public function delete(){
    $post = $this->input->post();
    return $this->db->delete($this->_table, array("email" => $post["email"]));
  }
}