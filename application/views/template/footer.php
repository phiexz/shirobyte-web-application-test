<footer class="footer bg-teal-gradient text-white">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg text-center text-lg-left">
        <span><strong>[v<?php echo $this->config->item("curr_ver"); ?>]</strong> made with love by <a href="https://phiexz.com"><strong>phiexz</strong></a></span>
      </div>
      <div class="col-12 col-lg text-center text-lg-right">
        <span>Site generated in </span><span id="execution-time" class="px-1 py-1 border"><?php echo $this->benchmark->elapsed_time();?></span><span> seconds</span>
      </div>
    </div>
  </div>
</footer>
