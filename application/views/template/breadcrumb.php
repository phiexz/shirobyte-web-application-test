<div class="shadow-1 bg-white fixed-top px-breadcrumb">
  <b-container>
    <b-breadcrumb class="mb-0">
      <span class="mr-2">You are here:</span>
      <b-breadcrumb-item active>
        <i class="fa fa-home"></i> 
      </b-breadcrumb-item>
      <b-breadcrumb-item active>
         <?php echo $title;?>
      </b-breadcrumb-item>
    </b-breadcrumb>
  </b-container>
</div>