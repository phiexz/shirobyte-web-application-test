<div id="header" class="jumbotron jumbotron-fluid parallax mb-0 bg-teal-gplay text-white">
  <div class="container">
    <div>
      <h1 class="display-3 font-weight-bold"> <?php echo $header;?></h1>
      <p class="lead"> <?php echo $subHeader;?></p>
      <?php
        if(isset($headerAlert)){
          echo '<b-alert show class="alert-modern warning shadow-1">'.$headerAlert.'</b-alert>';
        }
      ?>
      
    </div>
  </div>
</div>
