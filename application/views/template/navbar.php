<b-navbar toggleable="lg" type="dark" fixed="top" class="bg-teal-gradient" class="text-white">
  <b-container>
    <b-navbar-brand id="nav-home" href="/" v-b-tooltip.hover title="Back to Home"><i class="fa fa-hdd-o px-nav-icon"></i>shirobyte test</b-navbar-brand>
    <b-navbar-toggle target="navbar-collapse"></b-navbar-toggle>
    <b-collapse is-nav id="navbar-collapse">
      <!-- Right aligned nav items -->
      <b-navbar-nav class="ml-auto">
        <b-nav-item id="nav-register" href="register" v-b-tooltip.hover title="User Register" v-b-modal.modal-about key="navbarRegister">
          <i class="fa fa-user-plus px-nav-icon"></i>Register
        </b-nav-item>
        <b-nav-item id="nav-user-list" href="list" v-b-tooltip.hover title="List All User" v-b-modal.modal-about key="navbarListUser">
          <i class="fa fa-users px-nav-icon"></i>User List
        </b-nav-item>
        <b-nav-item id="nav-about" v-b-tooltip.hover title="About site" v-b-modal.modal-about key="navbarAbout">
          <i class="fa fa-info-circle px-nav-icon"></i>About
        </b-nav-item>
      </b-navbar-nav>
    </b-collapse>
  </b-container>
</b-navbar>