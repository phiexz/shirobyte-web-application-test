<!-- Modal About-->
<b-modal id="modal-about" ref="modalAbout" title="About site" ok-only ok-title="Close">
  <div>
    <h3 class="font-weight-bold">Shirobyte web application test</h3>
    <p class="lead">Version: <?php echo $this->config->item("curr_ver"); ?></p>
    <div class="mb-2">
      <img src="https://forthebadge.com/images/badges/built-with-love.svg" alt="built-with-love" />
      <img src="https://forthebadge.com/images/badges/powered-by-electricity.svg" alt="powered-by-electricity" />
    </div>
  </div>
  <div>
    <strong>Made possible by using this library:</strong>
    <ul>
      <li v-for="item in library">
        <a v-bind:href="item.link" target="_blank">{{ item.name }}</a>
      </li>
    </ul>
  </div>
</b-modal>