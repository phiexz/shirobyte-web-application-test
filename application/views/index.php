<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("template/head.php") ?>
  <?php $this->load->view("template/favicon.php") ?>
  <?php $this->load->view("template/css.php") ?>
</head>
<body style="background-image: url('<?php echo base_url(); ?>assets/img/leaves.png')">
  <div id="app">
    <?php $this->load->view("template/navbar.php") ?>
    <?php $this->load->view("template/breadcrumb.php") ?>
    <?php $this->load->view("template/header.php") ?>
    <?php $this->load->view("template/footer.php") ?>
    <?php $this->load->view("template/modals.php") ?>
  </div>

  
  <?php $this->load->view("template/js.php") ?>
</body>
</html>