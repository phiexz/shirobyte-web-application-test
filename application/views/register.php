<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("template/head.php") ?>
  <?php $this->load->view("template/favicon.php") ?>
  <?php $this->load->view("template/css.php") ?>
</head>
<body style="background-image: url('<?php echo base_url(); ?>assets/img/leaves.png')">
  <div id="app">
    <?php $this->load->view("template/navbar.php") ?>
    <?php $this->load->view("template/breadcrumb.php") ?>
    <?php $this->load->view("template/header.php") ?>
    <div class="jumbotron jumbotron-fluid parallax pt-1 mb-0" style="background-image: url('<?php echo base_url(); ?>assets/img/leaves.png')">
      <div class="container pb-4">
        <div class="card shadow-3 mb-2 offside-element">
          <div class="container p-5">
            <b-form id="form-register" @submit="registerSubmit" @reset="registerReset">
              <!-- email -->
              <b-form-group label="Email address:" label-for="input-email">
                <b-form-input
                  id="input-email"
                  v-model="form.email"
                  type="email"
                  required
                  placeholder="Enter email"
                ></b-form-input>
              </b-form-group>
              <!-- password -->
              <b-form-group label="Password:" label-for="input-password">
                <b-form-input
                  id="input-password"
                  v-model="form.password"
                  type="password"
                  required
                  placeholder="Enter password"
                ></b-form-input>
              </b-form-group>
              <!-- password 2 -->
              <b-form-group label="Retype Password:" label-for="input-password2">
                <b-form-input
                  id="input-password2"
                  v-model="form.password2"
                  type="password"
                  required
                  placeholder="Retype password"
                ></b-form-input>
              </b-form-group>
              <!-- name -->
              <b-form-group label="Your Name:" label-for="input-name">
                <b-form-input
                  id="input-name"
                  v-model="form.name"
                  required
                  placeholder="Enter name"
                ></b-form-input>
              </b-form-group>
              <!-- gender -->
              <b-form-group label="Gender:" label-for="input-gender">
                <b-form-radio v-model="form.gender" name="radio-gender" value="male" required>Male</b-form-radio>
                <b-form-radio v-model="form.gender" name="radio-gender" value="female" required>Female</b-form-radio>
              </b-form-group>
              <!-- telp number -->
              <b-form-group label="Telephone number:" label-for="input-telephone">
                <b-form-input
                  id="input-telephone"
                  v-model="form.telephone"
                  required
                  placeholder="Enter telephone"
                ></b-form-input>
              </b-form-group>
              <!-- occupation -->
              <b-form-group label="Occupation:" label-for="input-occupation">
                <b-form-select
                  id="input-occupation"
                  v-model="form.occupation"
                  :options="occupation"
                  required
                ></b-form-select>
              </b-form-group>
              <!-- photo -->
              <b-form-group label="Upload photo:" label-for="input-photo">
                <b-form-file id="input-photo" v-model="form.photo" class="mt-3" accept="image/jpeg, image/png" plain required></b-form-file>
              </b-form-group>

              <!-- submit/reset -->
              <b-button class="float-right mx-1 shadow-1" type="submit" variant="primary">Submit</b-button>
              <b-button class="float-right mx-1 shadow-1" type="reset" variant="danger">Reset</b-button>
            </b-form>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view("template/footer.php") ?>
    <?php $this->load->view("template/modals.php") ?>
  </div>

  
  <?php $this->load->view("template/js.php") ?>
</body>
</html>