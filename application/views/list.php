<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("template/head.php") ?>
  <?php $this->load->view("template/favicon.php") ?>
  <?php $this->load->view("template/css.php") ?>
</head>
<body style="background-image: url('<?php echo base_url(); ?>assets/img/leaves.png')">
  <div id="app">
    <?php $this->load->view("template/navbar.php") ?>
    <?php $this->load->view("template/breadcrumb.php") ?>
    <?php $this->load->view("template/header.php") ?>
    <div class="jumbotron jumbotron-fluid parallax pt-1 mb-0" style="background-image: url('<?php echo base_url(); ?>assets/img/leaves.png')">
      <div class="container pb-4">
        <div class="card shadow-3 mb-2 offside-element">
          <div class="container p-5">
            <table id="table-user" class="display" style="width:100%">
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Telephone number</th>
                  <th>Occupation</th>
                  <th>Delete user</th>
                </tr>
              </thead>
          </table>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view("template/footer.php") ?>
    <?php $this->load->view("template/modals.php") ?>
  </div>

  
  <?php $this->load->view("template/js.php") ?>
</body>
</html>