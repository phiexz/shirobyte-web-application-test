# Shirobyte Web Application Test

Web application for adding and listing users using sqlite database. 

## Why sqlite? 
Because it's **portability** and this web application not really needed full database server to run

## Framework and Library
Made possible by using:
- [Codeigniter](https://www.codeigniter.com)
- [Vue js](https://vuejs.org)
- [Bootstrap Vue](https://bootstrap-vue.js.org) 
- [Bootswatch Materia Theme](https://bootswatch.com)
- [Font Awesome](https://fontawesome.com/) 
- [jQuery](https://jquery.com/) 
- [jQuery DataTable](https://datatables.net/)


## Contact Me
- email: dhiika@phiexz.com
- phone: 081295360036 
- [github](https://github.com/phiexz) 
- [Facebook](https://fb.me/phiexz)
