var app = new Vue({
	el: "#app",
	data: {
		form: {
			email: "",
			password: "",
			password2: "",
			name: "",
			gender: "",
			telephone: "",
			occupation: null,
			photo: null
		},
		occupation: [{ text: "Select One", value: null }, "Karyawan swasta", "Pegawai negri", "Belum bekerja"],
		library: [
			{ name: "Codeigniter", link: "https://codeigniter.com/" },
			{ name: "Vue JS", link: "https://vuejs.org" },
			{ name: "Bootstrap Vue", link: "https://bootstrap-vue.js.org" },
			{ name: "Bootswatch Materia Theme", link: "https://bootswatch.com" },
			{ name: "Font Awesome", link: "https://fontawesome.com/" },
			{ name: "jQuery", link: "https://jquery.com/" },
			{ name: "jQuery DataTables", link: "https://datatables.net/" }
		]
	},
	methods: {
		registerSubmit(evt) {
			evt.preventDefault();
			//check if password match
			if (this.form.password != this.form.password2) {
				alert("password don't match");
			} else {
				//alert(JSON.stringify(this.form));
				//console.log(this.form);
				$.ajax({
					type: "POST",
					url: "users/add",
					data: {
						email: this.form.email,
						password: this.form.password,
						name: this.form.name,
						gender: this.form.gender,
						telephone: this.form.telephone,
						occupation: this.form.occupation
					},
					success: function(msg) {
						if (msg == "success") {
							alert("Registration success.");
							//reset form
							app.registerReset();
						} else if (msg == "exist") {
							alert("Email address already exist.\nPlease use another email address.");
						} else {
							alert("Ooops.. registration failed.");
						}
					}
				});
			}
		},
		registerReset() {
			//evt.preventDefault();
			// Reset our form values
			this.form.email = "";
			this.form.password = "";
			this.form.password2 = "";
			this.form.name = "";
			this.form.gender = "";
			this.form.telephone = "";
			this.form.occupation = null;
			this.form.photo = null;
			// Trick to reset/clear native browser form validation state
			this.show = false;
			this.$nextTick(() => {
				this.show = true;
			});
		}
	},
	created() {},
	mounted() {}
});

$(document).ready(function() {
	//get how many user
	$.get("users/count").done(function(data) {
		$("#count-users").text(data);
	});
	var table = $("#table-user").DataTable({
		ajax: { url: "users/get", dataSrc: "" },
		columns: [
			{ data: "email" },
			{ data: "name" },
			{ data: "gender" },
			{ data: "telephone" },
			{ data: "occupation" },
			{ data: null, defaultContent: '<button type="button" class="btn btn-danger btn-sm">Delete</button>' }
		],
		pageLength: 20,
		order: [[1, "asc"]]
	});

	$("#table-user tbody").on("click", "button", function() {
		var data = table.row($(this).parents("tr")).data();
		//console.log(data.email);
		if (confirm("Are you sure want to delete?")) {
			//send ajax post
			$.ajax({
				type: "POST",
				url: "users/delete",
				data: data,
				success: function(msg) {
					if (msg == "success") {
						alert("User deleted");
						//refresh table
						table.ajax.reload();
					} else {
						alert("Ooops.. failed to delete user.");
					}
				}
			});
		} else {
			//do nothing
		}
	});
});
